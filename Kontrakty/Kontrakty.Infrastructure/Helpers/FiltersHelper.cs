﻿using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Helpers
{
    /// <summary>
    /// The filters helper class
    /// </summary>
    public class FiltersHelper
    {
        /// <summary>
        /// The empty filters
        /// </summary>
        private static readonly FiltersHelper emptyFilters;

        /// <summary>
        /// Initializes a new instance of the <see cref="FiltersHelper"/> class
        /// </summary>
        static FiltersHelper()
        {
            emptyFilters = new FiltersHelper();
        }

        /// <summary>
        /// The name filter
        /// </summary>
        public string NameFilter { get; set; }

        /// <summary>
        /// The type filter
        /// </summary>
        public ContractType? TypeFilter { get; set; }

        /// <summary>
        /// The experience filter
        /// </summary>
        public int? ExperienceFilter { get; set; }

        /// <summary>
        /// The salary filter
        /// </summary>
        public decimal? SalaryFilter { get; set; }

        /// <summary>
        /// Indicates if should choose only developers with 5 year experience
        /// </summary>
        public bool? DevWithExperience { get; set; }

        /// <summary>
        /// indicates if should choose only contracts with high salary
        /// </summary>
        public bool? HighSalary { get; set; }

        /// <summary>
        /// The empty filters
        /// </summary>
        public static FiltersHelper Empty
        {
            get
            {
                return emptyFilters;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FiltersHelper))
                return false;

            FiltersHelper second = obj as FiltersHelper;

            if (this.NameFilter != second.NameFilter)
                return false;
            if (this.SalaryFilter != second.SalaryFilter)
                return false;
            if (this.TypeFilter != second.TypeFilter)
                return false;
            if (this.HighSalary != second.HighSalary)
                return false;
            if (this.ExperienceFilter != second.ExperienceFilter)
                return false;
            if (this.DevWithExperience != second.DevWithExperience)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
