﻿using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Helpers;
using Kontrakty.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Logic
{
    /// <summary>
    /// The contract filter
    /// </summary>
    public class ContractFilter : IContractFilter
    {
        /// <summary>
        /// The exprience
        /// </summary>
        private const int experience = 5;

        /// <summary>
        /// The salary
        /// </summary>
        private const decimal salary = 5000;

        /// <summary>
        /// Applies filters to passed objects
        /// </summary>
        /// <param name="filters">The filters</param>
        /// <param name="objects">The objects list</param>
        /// <returns>The <see cref="IEnumerable{Contract}"/>.</returns>
        public IEnumerable<Contract> ApplyFilters(FiltersHelper filters, IEnumerable<Contract> objects)
        {
            bool missExpAndType = false;
            bool missSalary = false;

            if (filters.DevWithExperience.HasValue && filters.DevWithExperience.Value)
            {
                objects = objects.Where(x => x.Type.Equals(ContractType.Developer) && x.Experience.Equals(experience));
                missExpAndType = true;
            }

            if (filters.HighSalary.HasValue && filters.HighSalary.Value)
            {
                objects = objects.Where(x => x.Salary > salary);
                missSalary = true;
            }

            if (!string.IsNullOrEmpty(filters.NameFilter))
            {
                objects = objects.Where(x => x.Name.Contains(filters.NameFilter));
            }

            if (!missExpAndType && filters.TypeFilter != null)
            {
                objects = objects.Where(x => x.Type.Equals(filters.TypeFilter.Value));
            }

            if (!missExpAndType && filters.ExperienceFilter.HasValue)
            {
                objects = objects.Where(x => x.Experience.Equals(filters.ExperienceFilter.Value));
            }

            if (!missSalary && filters.SalaryFilter.HasValue)
            {
                objects = objects.Where(x => x.Salary.Equals(filters.SalaryFilter.Value));
            }

            return objects;
        }
    }
}
