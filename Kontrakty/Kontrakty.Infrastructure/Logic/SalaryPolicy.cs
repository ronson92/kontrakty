﻿using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Logic
{
    /// <summary>
    /// The salary policy
    /// </summary>
    public class SalaryPolicy : ISalaryPolicy
    {
        /// <summary>
        /// Calculates salary
        /// </summary>
        /// <param name="experience">The experience</param>
        /// <param name="type">The contract type</param>
        /// <returns>The <see cref="double"/></returns>
        public double CalculateSalary(int experience, ContractType type)
        {
            return type.Equals(ContractType.Developer) ? DeveloperSalary(experience) : TesterSalary(experience);
        }

        /// <summary>
        /// The developer salary
        /// </summary>
        /// <param name="experience">The experience</param>
        /// <returns>The <see cref="double"/></returns>
        private double DeveloperSalary(int experience)
        {
            double minPrice = 0.0;

            if (experience >= 1 && experience < 3)
                minPrice = 2500;
            if (experience >= 3 && experience <= 5)
                minPrice = 5000;
            if (experience > 5)
                minPrice = 5500;

            return minPrice + (experience * 125);
        }

        /// <summary>
        /// The tester salary
        /// </summary>
        /// <param name="experience">The experience</param>
        /// <returns>The <see cref="double"/></returns>
        private double TesterSalary(int experience)
        {
            double minPrice = 0.0;

            if (experience >= 1 && experience < 2)
                minPrice = 2000;
            if (experience >= 2 && experience <= 4)
                minPrice = 2700;
            if (experience > 4)
                minPrice = 3200;

            return minPrice + (experience * 100 + (minPrice / 4));
        }
    }
}
