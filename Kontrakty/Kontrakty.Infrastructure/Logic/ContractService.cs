﻿using Kontrakty.Database.Interfaces;
using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Helpers;
using Kontrakty.Infrastructure.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Logic
{
    /// <summary>
    /// The contracts service
    /// </summary>
    public class ContractService : IContractService
    {
        /// <summary>
        /// The items per page count
        /// </summary>
        private int itemsPerPage;

        /// <summary>
        /// The contracts repository
        /// </summary>
        private IContractsRepository contractsRepository;

        /// <summary>
        /// The contract filter
        /// </summary>
        private IContractFilter contractFilter;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractService"/> class
        /// </summary>
        /// <param name="contractsRepository">The contracts repository</param>
        /// <param name="contractFilter">The contract filter</param>
        public ContractService(IContractsRepository contractsRepository, IContractFilter contractFilter)
        {
            this.contractsRepository = contractsRepository;
            this.contractFilter = contractFilter;
            itemsPerPage = 10;
        }

        /// <summary>
        /// Adds new contract to database
        /// </summary>
        /// <param name="contract">The contract</param>
        public void AddNewContract(Contract contract)
        {
            contractsRepository.Insert(contract);
            contractsRepository.Save();
        }

        /// <summary>
        /// Gets contracts by page
        /// </summary>
        /// <param name="filters">The filters</param>
        /// <param name="page">The page number</param>
        /// <returns>The <see cref="IPagedList{Contract}"/>.</returns>
        public IPagedList<Contract> GetContracts(FiltersHelper filters, int? page)
        {
            var allObj = contractsRepository.GetAll();
            var objects = contractFilter.ApplyFilters(filters, allObj);
            return objects.ToPagedList<Contract>(page ?? 1, itemsPerPage);
        }   
    }
}
