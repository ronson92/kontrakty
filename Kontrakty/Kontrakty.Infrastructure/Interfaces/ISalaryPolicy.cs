﻿using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Interfaces
{
    /// <summary>
    /// The salary policy interface
    /// </summary>
    public interface ISalaryPolicy
    {
        /// <summary>
        /// Calculates salary
        /// </summary>
        /// <param name="experience">The experience</param>
        /// <param name="type">The contract type</param>
        /// <returns>The <see cref="double"/></returns>
        double CalculateSalary(int experience, ContractType type);
    }
}
