﻿using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Helpers;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Interfaces
{
    /// <summary>
    /// The interface for contracts service
    /// </summary>
    public interface IContractService
    {
        /// <summary>
        /// Adds new contract to database
        /// </summary>
        /// <param name="contract">The contract</param>
        void AddNewContract(Contract contract);

        /// <summary>
        /// Gets contracts by page
        /// </summary>
        /// <param name="filters">The filters</param>
        /// <param name="page">The page number</param>
        /// <returns>The <see cref="IPagedList{Contract}"/>.</returns>
        IPagedList<Contract> GetContracts(FiltersHelper filters, int? page);
    }
}
