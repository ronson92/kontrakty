﻿using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Infrastructure.Interfaces
{
    /// <summary>
    /// The contract filter interface
    /// </summary>
    public interface IContractFilter
    {
        /// <summary>
        /// Applies filters to passed objects
        /// </summary>
        /// <param name="filters">The filters</param>
        /// <param name="objects">The objects list</param>
        /// <returns>The <see cref="IEnumerable{Contract}"/>.</returns>
        IEnumerable<Contract> ApplyFilters(FiltersHelper filters, IEnumerable<Contract> objects);
    }
}
