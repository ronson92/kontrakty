﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Mapper
{
    /// <summary>
    /// The interface for the mapper.
    /// </summary>
    public interface IMap
    {
        /// <summary>
        /// Maps one type to another.
        /// </summary>
        /// <typeparam name="TDestination">The destination type.</typeparam>
        /// <typeparam name="TSource">The source type.</typeparam>
        /// <param name="source">The source object.</param>
        /// <returns>The destination-type object.</returns>
        TDestination Map<TSource, TDestination>(TSource source);
    }
}
