﻿using Kontrakty.Models;
using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Kontrakty.Mapper
{
    /// <summary>
    /// The mapper class
    /// </summary>
    public class Mapper : IMap
    {
        /// <summary>
        /// The mapper
        /// </summary>
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="Mapper"/> class.
        /// </summary>
        public Mapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Contract, ContractModel>();
                cfg.CreateMap<ContractModel, Contract>();
            });
            mapper = config.CreateMapper();
        }

        /// <summary>
        /// Maps one type to another.
        /// </summary>
        /// <typeparam name="TDestination">The destination type.</typeparam>
        /// <typeparam name="TSource">The source type.</typeparam>
        /// <param name="source">The source object.</param>
        /// <returns>The destination-type object.</returns>
        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return mapper.Map<TSource, TDestination>(source);
        }
    }
}