﻿using Kontrakty.Database;
using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kontrakty.Models
{
    /// <summary>
    /// The contract model class
    /// </summary>
    public class ContractModel
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [Required(ErrorMessage = "Nazwa kontraktu jest wymagana")]
        [Display(Name="Nazwa kontraktu")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the contract type
        /// </summary>
        [Required(ErrorMessage = "Typ kontraktu jest wymagany")]
        [Display(Name = "Typ kontraktu")]
        public ContractType Type { get; set; }

        /// <summary>
        /// Gets or sets the experience
        /// </summary>
        [Required(ErrorMessage = "Doświadczenie jest wymagane")]
        [Range(0, int.MaxValue)]
        [Display(Name = "Doświadczenie")]
        public int Experience { get; set; }

        /// <summary>
        /// Gets or sets the salary
        /// </summary>
        [Required(ErrorMessage="Stawka jest wymagana")]
        [CustomValidation(typeof(CustomValidators), "ValidateSalary")]
        [Display(Name = "Stawka (zł)")]
        public decimal Salary { get; set; }
    }
}