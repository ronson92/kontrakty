﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontrakty.Autofac
{
    /// <summary>
    /// The Autofac config class
    /// </summary>
    public class AutofacConfig
    {
        /// <summary>
        /// Configure autofac container
        /// </summary>
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();            
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterFilterProvider();
            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterModule(new DataModule());

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}