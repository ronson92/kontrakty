﻿using Autofac;
using Kontrakty.Database.Interfaces;
using Kontrakty.Database.Logic;
using Kontrakty.Infrastructure.Interfaces;
using Kontrakty.Infrastructure.Logic;
using Kontrakty.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kontrakty.Autofac
{
    /// <summary>
    /// The data module class
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Adds registration to the container
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterType<ContractsContext, IDbContext>(builder);
            RegisterType<ContractsRepository, IContractsRepository>(builder);
            RegisterType<ContractService, IContractService>(builder);
            RegisterType<Kontrakty.Mapper.Mapper, Kontrakty.Mapper.IMap>(builder);
            RegisterType<ContractFilter, IContractFilter>(builder);
            RegisterType<SalaryPolicy, ISalaryPolicy>(builder);
            base.Load(builder);
        }

        /// <summary>
        /// Registers type
        /// </summary>
        /// <typeparam name="TImplementer">The implementer type</typeparam>
        /// <typeparam name="TService">The service type</typeparam>
        /// <param name="builder">The builder</param>
        private void RegisterType<TImplementer, TService>(ContainerBuilder builder)
        {
            builder.RegisterType<TImplementer>().As<TService>().InstancePerRequest();
        }
    }
}