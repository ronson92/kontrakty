﻿using Kontrakty.Mapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontrakty.Controllers
{
    /// <summary>
    /// The base controller
    /// </summary>
    public class BaseController<TBusinessLogic> : Controller
    {
        /// <summary>
        /// Gets or sets the business logic
        /// </summary>
        public TBusinessLogic BusinessLogic { get; private set; }

        /// <summary>
        /// Gets or sets the logger
        /// </summary>
        public ILog Logger { get; private set; }

        /// <summary>
        /// Gets or sets the mapper
        /// </summary>
        public IMap Mapper { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController"/> class.
        /// </summary>
        /// <param name="businessLogic">The business logic</param>
        /// <param name="mapper">The mapper</param>
        public BaseController(TBusinessLogic businessLogic, IMap mapper)
        {
            this.BusinessLogic = businessLogic;
            this.Mapper = mapper;
            log4net.Config.XmlConfigurator.Configure();
            Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
    }
}