﻿using Kontrakty.Database.Interfaces;
using Kontrakty.Database.Models;
using Kontrakty.Infrastructure.Helpers;
using Kontrakty.Infrastructure.Interfaces;
using Kontrakty.Mapper;
using Kontrakty.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontrakty.Controllers
{
    /// <summary>
    /// The home controller
    /// </summary>
    public class HomeController : BaseController<IContractService>
    {
        /// <summary>
        /// The contracts repository
        /// </summary>
        private readonly IContractsRepository contractsRepository;

        /// <summary>
        /// The salary policy
        /// </summary>
        private readonly ISalaryPolicy salaryPolicy;

        /// <summary>
        /// Initialzies na new instance of the <see cref="HomeController"/> class
        /// </summary>
        /// <param name="businessLogic">The business logic</param>
        /// <param name="mapper">The mapper</param>
        /// <param name="contractsRepository">The contracts repository</param>
        /// <param name="salaryPolicy">The salary policy</param>
        public HomeController(IContractService businessLogic, 
            IMap mapper, 
            IContractsRepository contractsRepository,
            ISalaryPolicy salaryPolicy)
            : base(businessLogic, mapper)
        {
            this.contractsRepository = contractsRepository;
            this.salaryPolicy = salaryPolicy;
        }

        /// <summary>
        /// The index action
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        public ActionResult Index(int? page, FiltersHelper filters)
        {
            if (!filters.Equals(FiltersHelper.Empty))
            {
                Session["Filters"] = filters;
            }

            var currentFilters = (Session["Filters"] as FiltersHelper) ?? FiltersHelper.Empty;

            ViewBag.NameFilter = currentFilters.NameFilter;
            ViewBag.TypeFilter = currentFilters.TypeFilter;
            ViewBag.ExperienceFilter = currentFilters.ExperienceFilter;
            ViewBag.SalaryFilter = currentFilters.SalaryFilter;
            ViewBag.devWithExperience = currentFilters.DevWithExperience;
            ViewBag.highSalary = currentFilters.HighSalary;

            var currentList = BusinessLogic.GetContracts(currentFilters, page);
            ViewBag.CurrentList = currentList;

            return Request.IsAjaxRequest()
                ? (ActionResult)PartialView("UnobtrusiveAjax_Partial", currentList)
                : View();
        }

        /// <summary>
        /// The add contract action
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpGet]
        public ActionResult AddContract()
        {
            return View();
        }

        /// <summary>
        /// The add contract action
        /// </summary>
        /// <param name="model">The model</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult AddContract(ContractModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var entity = Mapper.Map<ContractModel, Contract>(model);
            try
            {
                BusinessLogic.AddNewContract(entity);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Wystąpił błąd podczas dodawania kontraktu do bazy");
                Logger.Error("HomeController, [POST] AddContract", ex);
                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Calculates salary
        /// </summary>
        /// <param name="experience">The experience</param>
        /// <param name="type">The type</param>
        /// <returns>The <see cref="decimal"/>.</returns>
        [HttpGet]
        public ActionResult CalculateSalary(string experience, ContractType type)
        {
            int exp = string.IsNullOrEmpty(experience) ? 0 : int.Parse(experience);
            double salary = salaryPolicy.CalculateSalary(exp, type);
            return Json(salary, JsonRequestBehavior.AllowGet);
        }
    }
}