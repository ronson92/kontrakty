﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Interfaces
{
    /// <summary>
    /// The interface for database context
    /// </summary>
    public interface IDbContext : IDisposable
    {
        /// <summary>
        /// Gets database set of passed type
        /// </summary>
        /// <typeparam name="TEntity">The entities type</typeparam>
        /// <returns>The <see cref="IDbSet{TEntity}"/>.</returns>
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Gets an entry of entity
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <returns>The <see cref="DbEntityEntry"/></returns>
        DbEntityEntry Entry(object entity);

        /// <summary>
        /// Saves changes
        /// </summary>
        /// <returns>The save result</returns>
        int SaveChanges();
    }
}
