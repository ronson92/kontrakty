﻿using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Interfaces
{
    /// <summary>
    /// The interface for the contracts repository
    /// </summary>
    public interface IContractsRepository : IRepository<Contract>
    {
        /// <summary>
        /// Gets contract by name
        /// </summary>
        /// <param name="name">The contract name</param>
        /// <returns>The <see cref="Contract"/>.</returns>
        Contract GetContractByName(string name);
    }
}
