﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Interfaces
{
    /// <summary>
    /// The interface for repository
    /// </summary>
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <typeparam name="TEntity">The entities type</typeparam>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Inserts entity
        /// </summary>
        /// <param name="entity">The entity</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entity">The entity</param>
        void Update(TEntity entity);

        /// <summary>
        /// Saves changes
        /// </summary>
        void Save();
    }
}
