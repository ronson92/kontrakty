﻿using Kontrakty.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Logic
{
    /// <summary>
    /// The repository
    /// </summary>
    /// <typeparam name="TEntity">The entities type</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly IDbContext context;

        /// <summary>
        /// Value indicating if object already disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public Repository(IDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns>The <see cref="IEnumerable{TEntity}"/></returns>
        public IEnumerable<TEntity> GetAll()
        {
            return GetEntities().ToList();
        }

        /// <summary>
        /// Inserts entity
        /// </summary>
        /// <param name="entity">The entity</param>
        public void Insert(TEntity entity)
        {
            GetEntities().Add(entity);
        }

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity)
        {
            GetEntities().Remove(entity);
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entity">The entity</param>
        public void Update(TEntity entity)
        {
            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        /// <summary>
        /// Saves changes
        /// </summary>
        public void Save()
        {
            context.SaveChanges();
        }

        /// <summary>
        /// Dispose object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose object
        /// </summary>
        /// <param name="disposing">The disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Gets entities
        /// </summary>
        /// <returns>The <see cref="IDbSet{TEntity}"/>.</returns>
        private IDbSet<TEntity> GetEntities()
        {
            return this.context.Set<TEntity>();
        }
    }
}
