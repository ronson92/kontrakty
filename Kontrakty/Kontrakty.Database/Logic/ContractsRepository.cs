﻿using Kontrakty.Database.Interfaces;
using Kontrakty.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Logic
{
    /// <summary>
    /// The contracts repository
    /// </summary>
    public class ContractsRepository : Repository<Contract>, IContractsRepository
    {
        /// <summary>
        /// The context.
        /// </summary>
        private IDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractsRepository"/> class.
        /// </summary>
        /// <param name="context">The database context</param>
        public ContractsRepository(IDbContext context)
            : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets contract by name
        /// </summary>
        /// <param name="name">The contract name</param>
        /// <returns>The <see cref="Contract"/>.</returns>
        public Models.Contract GetContractByName(string name)
        {
            return GetAll().Where(x => x.Name.Equals(name)).SingleOrDefault();
        }
    }
}
