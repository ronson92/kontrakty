﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database
{
    /// <summary>
    /// The custom validators class
    /// </summary>
    public class CustomValidators
    {
        /// <summary>
        /// The wrong salary message
        /// </summary>
        private const string WrongSalary = "Pole Wypłata musi zawierać wartość większą lub równą 0.";

        /// <summary>
        /// Validates salary
        /// </summary>
        /// <param name="value">The salary value</param>
        /// <param name="context">The validation context</param>
        /// <returns>The <see cref="ValidationResult"/></returns>
        public static ValidationResult ValidateSalary(decimal value, ValidationContext context)
        {
            if (value < decimal.Zero)
            {
                return new ValidationResult(WrongSalary);
            }

            return ValidationResult.Success;
        }
    }
}
