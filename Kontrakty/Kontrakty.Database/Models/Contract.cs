﻿using Kontrakty.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrakty.Database.Models
{
    public enum ContractType
    {
        [Display(Name="Programista")]
        Developer, 

        [Display(Name="Tester")]
        SoftwareTester
    }

    public class Contract
    {
        [Key]
        public string Name { get; set; }

        [Required]
        public ContractType Type { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int Experience { get; set; }

        [Required]
        [CustomValidation(typeof(CustomValidators), "ValidateSalary")]
        public decimal Salary { get; set; }
    }
}
